import React from 'react'
import style from './header.module.css'
import {Link} from 'react-router-dom'
import FontAwesome from 'react-fontawesome'
import SideNavigation from './Sidenav/sidenav'


const Header = (props) => {

    const navBars = () => (
        <div>
            <FontAwesome
                onClick={props.onOpenNav}
                className={style.navbar}
                name="bars"

            />
        </div>
    )


    const logo = () => {
        return (
            <Link to="/" className={style.logo}>
                <img alt="nba-logo" src="/images/nba_logo.png"/>
            </Link>
        )
    }

    return (
        <header className={style.header}>
            <SideNavigation {...props}/>
            <div className={style.container}>
                {navBars()}
                {logo()}
            </div>
        </header>
    )
}

export default Header
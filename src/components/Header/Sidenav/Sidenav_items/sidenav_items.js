import React from 'react'
import {Link} from 'react-router-dom'
import FontAwesome from 'react-fontawesome'
import style from './sidenav_items.module.css'

const SideNavItems = () => {

    const items = [
        {
            type: style.nav_option,
            icon: 'home',
            text: 'Home',
            link: '/'
        },
        {
            type: style.nav_option,
            icon: 'newspaper',
            text: 'News',
            link: '/news'
        },
        {
            type: style.nav_option,
            icon: 'play',
            text: 'Videos',
            link: '/videos'
        },
        {
            type: style.nav_option,
            icon: 'greater-than',
            text: 'Sign in',
            link: '/sign-in'
        },
        {
            type: style.nav_option,
            icon: 'window-close',
            text: 'Sign out',
            link: '/sign-out'
        }
    ]

    const showItems = () => {
        return items.map((item, i) => {
            return (
                <div key={i} className={item.type}>
                    <Link to={item.link}>
                        <FontAwesome name={item.icon}/>
                        {item.text}
                    </Link>
                </div>
            )
        })
    }


    return (
        <div>
            {showItems()}
        </div>
    )
}

export default SideNavItems
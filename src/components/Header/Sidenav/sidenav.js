import React from 'react'
import SideNav from 'react-simple-sidenav'
import SideNavItems from './Sidenav_items/sidenav_items'

const SideNavigation = (props) => {
    return (
        <div>
            <SideNav
                showNav={props.showNav}
                onHideNav={props.onHideNav}
                navStyle={{
                    background: '#242424',
                    maxWidth: '70%'
                }}
            >
                <SideNavItems/>
            </SideNav>
        </div>
    )
}

export default SideNavigation
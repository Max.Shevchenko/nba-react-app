import React from 'react'
import TeamInfo from '../../Articles/Elems/teamInfo'

const HeaderArticle = (props) => {

    const teamInfo = (team) => {
        return team ? (
            <TeamInfo team={team}/>
        ) : null
    }

    return (
        <div>
            {teamInfo(props.teamData)}
        </div>
    )
}

export default HeaderArticle
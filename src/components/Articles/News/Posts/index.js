import React, {Component} from 'react'
import axios from 'axios'
import {URL} from '../../../../config'
import style from '../../articles.module.css'
import Header from "./header";

class NewsArticles extends Component {

    state = {
        articles: [],
        team: []
    }

    componentWillMount() {
        axios.get(`${URL}/articles?id=${this.props.match.params.id}`)
            .then(response => {
                let article = response.data[0]
                axios.get(`${URL}/teams?id=${article.team}`)
                    .then(
                        response => {
                            this.setState({
                                articles: [article],
                                team: response.data
                            })
                        }
                    )
            })
    }

    render() {

        const article = this.state.articles[0]
        const team = this.state.team

        return (
            <div className={style.wrapper}>
                <Header
                    teamData={team[0]}
                    date={article ? article.date : ''}
                    author={article ? article.author : ''}
                />
                <div className={style.articleBody}>
                    <h1>{article ? article.title : ''}</h1>
                    <div className={style.articleImg}
                         style={{
                             background: `url('/images/articles/${article ? article.image : ''}')`
                         }}
                    >
                        {article ? article.image : ''}
                    </div>
                    <div className={style.articleText}>
                        {article ? article.body : ''}
                    </div>
                </div>
            </div>
        )
    }
}

export default NewsArticles
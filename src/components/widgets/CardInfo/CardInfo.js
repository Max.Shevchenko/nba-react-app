import React from 'react'
import FontAwesome from 'react-fontawesome'
import style from './CardInfo.module.css'

const CardInfo = (props) => {

    const teamName = (teams, team) => {
        let data = teams.find(item => item.id === team)
        return data ? data.name : 'unknown'
    }

    return (
        <div
            className={style.card_info}

        >
            <span className={style.team_name}>
                {teamName(props.teams, props.team)}
            </span>
            <span className={style.date}>
                <FontAwesome name="clock"/>
                {props.date}
            </span>
        </div>
    )
}

export default CardInfo
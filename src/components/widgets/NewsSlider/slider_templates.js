import React from 'react'
import {Link} from 'react-router-dom'
import Slick from 'react-slick'
import style from './slider.module.css'

const SliderTemplates = (props) => {

    let template = null

    const settings = {
        dots: true,
        infinite: true,
        arrows: false,
        speed: 500,
        slidesToShow: 1,
        slidesToScroll: 1,
        autoplay: true,
        autoplaySpeed: 4000,
        ...props.settings
    }

    switch(props.type) {
        case ('featured'):
            template = props.data.map((item, i) => {
                return (
                    <div key={i}>
                        <div className={style.featured_item}>
                            <div className={style.featured_img}
                                style={{
                                    background: `url(../images/articles/${item.image})`
                                }}
                            >
                                <Link to={`/articles/${item.id}`}>
                                    <div className={style.featured_caption}>
                                        <span>
                                        {item.title}
                                        </span>
                                    </div>
                                </Link>
                            </div>
                        </div>
                    </div>
                )
            })
            break;
        default:
            template = null
    }

    return (
        <Slick {...settings}>
            {template}
        </Slick>
    )
}

export default SliderTemplates
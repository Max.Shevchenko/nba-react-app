import React from 'react'
import style from '../VideosList.module.css'
import VideosTemplate from "../VideosTemplate";

const VideosRelated = (props) => {
    return (
        <div className={style.relatedWrapper}>
            <VideosTemplate
                data={props.data}
                teams={props.teams}
            />
        </div>
    )
}

export default VideosRelated
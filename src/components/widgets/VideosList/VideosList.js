import React, {Component} from 'react'
import style from './VideosList.module.css'
import axios from 'axios'
import {URL} from '../../../config'
import Button from '../Button/button'
import VideosTemplate from './VideosTemplate'

class VideosList extends Component {

    state = {
        teams: [],
        videos: [],
        start: this.props.start,
        end: this.props.start + this.props.amount,
        amount: this.props.amount
    }

    request = (start, end) => {
        if (this.state.teams.length < 1) {
            axios.get(`${URL}/teams`)
                .then(responce => {
                    this.setState({
                        teams: responce.data
                    })
                })
        }

        axios.get(`${URL}/videos?_start=${start}&_end=${end}`)
            .then(responce => {
                this.setState({
                    videos: [...this.state.videos, ...responce.data],
                    start,
                    end
                })
            })
    }

    componentWillMount() {
        this.request(this.state.start, this.state.end)
    }

    renderVideos = () => {
        let template = null
        switch (this.props.type) {
            case ('card'):
                template = <VideosTemplate data={this.state.videos} teams={this.state.teams}/>
                break
            default:
                template = null
        }

        return template
    }

    loadMore = () => {
        let end = this.state.end + this.state.amount
        this.request(this.state.end, end)
    }

    renderTitle = () => this.props.title ? (<h3><strong>NBA</strong> Videos</h3>)
        : null

    renderButton = () => this.props.loadmore ? <Button
        type="loadmore"
        loadmore={() => this.loadMore()}
        cta="Load more videos"
        /> :
        <Button type="linkTo" cta="More videos" linkTo="/videos"/>

    render() {
        return (
            <div className={style.videoslist_wrapper}>
                {this.renderTitle()}
                {this.renderVideos()}
                {this.renderButton()}
            </div>
        )
    }
}

export default VideosList
import React from 'react'
import style from './VideosList.module.css'
import {Link} from 'react-router-dom'
import CardInfo from '../CardInfo/CardInfo'

const VideosTemplate = (props) => {
    return props.data.map((item, i) => {
        return (<Link key={i} to={`/videos/${item.id}`}>
            <div className={style.video_item_wrapper}>
                <div className={style.left} style={
                    {
                        background: `url(/images/videos/${item.image})`
                    }
                }>
                    <div></div>
                </div>
                <div className={style.right}>
                    <CardInfo teams={props.teams} team={item.team} date={item.date}/>
                    <h2>{item.title}</h2>
                </div>
            </div>
        </Link>)
    })
}

export default VideosTemplate